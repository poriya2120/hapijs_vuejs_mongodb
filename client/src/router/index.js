import Vue from 'vue'
import Router from 'vue-router'
import List from '@/components/List'
// import List from '../components/List.vue'
// import lise from '@/components/Li'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'List',
      component: List
    }
  ]
})
